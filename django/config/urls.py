from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('teglanna_app.urls')),
    url(r'^markitup/', include('markitup.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

