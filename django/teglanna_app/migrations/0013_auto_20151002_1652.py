# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0012_nagykepek'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BioItem',
        ),
        migrations.DeleteModel(
            name='LearningItem',
        ),
        migrations.DeleteModel(
            name='MenuItem',
        ),
    ]
