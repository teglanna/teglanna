# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import teglanna_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0008_auto_20150928_0619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectitem',
            name='pic',
            field=models.FileField(upload_to=teglanna_app.models.blog_kepek_filename, blank=True),
        ),
    ]
