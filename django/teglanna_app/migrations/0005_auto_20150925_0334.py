# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0004_learningitem'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='learningitem',
            name='title',
        ),
        migrations.RemoveField(
            model_name='learningitem',
            name='url',
        ),
    ]
