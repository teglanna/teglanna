# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0005_auto_20150925_0334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learningitem',
            name='slug',
            field=models.SlugField(),
        ),
    ]
