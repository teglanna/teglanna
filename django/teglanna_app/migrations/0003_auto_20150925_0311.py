# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0002_auto_20150925_0248'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tartalom',
            old_name='_szoveg_rendered',
            new_name='_text_rendered',
        ),
        migrations.RenameField(
            model_name='tartalom',
            old_name='szoveg',
            new_name='text',
        ),
    ]
