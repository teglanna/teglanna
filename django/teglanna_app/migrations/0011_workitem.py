# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import markitup.fields
import teglanna_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0010_auto_20150928_0652'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('pic', models.FileField(upload_to=teglanna_app.models.gallery_kepek_filename, blank=True)),
                ('description', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('order', models.IntegerField(null=True, blank=True)),
                ('_description_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
