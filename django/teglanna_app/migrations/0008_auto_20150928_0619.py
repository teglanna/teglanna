# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0007_project'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Project',
            new_name='ProjectItem',
        ),
    ]
