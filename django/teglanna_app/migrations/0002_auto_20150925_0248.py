# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import markitup.fields


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BioItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('title', models.CharField(max_length=100)),
                ('text', markitup.fields.MarkupField(no_rendered_field=True, blank=True)),
                ('order', models.IntegerField(null=True, blank=True)),
                ('_text_rendered', models.TextField(editable=False, blank=True)),
            ],
            options={
                'ordering': ['order'],
            },
        ),
        migrations.DeleteModel(
            name='Blog',
        ),
        migrations.DeleteModel(
            name='Feladat',
        ),
        migrations.DeleteModel(
            name='HomeItem',
        ),
        migrations.DeleteModel(
            name='Tematika',
        ),
    ]
