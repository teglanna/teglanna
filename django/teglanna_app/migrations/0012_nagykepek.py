# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import teglanna_app.models


class Migration(migrations.Migration):

    dependencies = [
        ('teglanna_app', '0011_workitem'),
    ]

    operations = [
        migrations.CreateModel(
            name='NagyKepek',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(unique=True)),
                ('kep_file', models.FileField(upload_to=teglanna_app.models.blog_kepek_filename, blank=True)),
            ],
        ),
    ]
