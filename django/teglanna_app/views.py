from django.shortcuts import render, get_object_or_404
from .models import Tartalom, Kepek, ProjectItem, WorkItem, NagyKepek



def home(request):    
    whoami = Tartalom.objects.get(slug="whoami")
#    before = Tartalom.objects.get(slug="before")
#    after = Tartalom.objects.get(slug="after")
    work_list = WorkItem.objects.order_by('order')
    project_list = ProjectItem.objects.order_by('order')
    social = Tartalom.objects.get(slug="social")
    pic_list = Kepek.objects.all()
    nagypic_list = NagyKepek.objects.all()
    context_dict = {
        'active': 'before',
#        'before': before,
#        'after': after,
        'whoami' : whoami,
        'pic_list': pic_list,
	'nagypic_list': nagypic_list,
        'project_list': project_list,
        'work_list': work_list,
        'social': social,

    }

    return render(request, 'teglanna_app/home.html', context_dict)



def some_pic(request):
    menu_list = MenuItem.objects.order_by('order')
    pic_list = Kepek.objects.all()

    context_dict = {
        'menu_list': menu_list,
        'active': 'some_pic',
        'pic_list': pic_list,
    }

    return render(request, 'teglanna_app/some_pic.html', context_dict)
