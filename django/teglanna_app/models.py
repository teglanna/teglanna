# -*- coding: utf-8 -*-

import os
from django.db import models
from markitup.fields import MarkupField


def gallery_kepek_filename(instance, filename):
        _, ext = os.path.splitext(filename)
        ext = ext.lower()
        basename = instance.slug
        dir = 'gallery_kepek'
        return os.path.join(dir, '{}{}'.format(basename, ext))


def blog_kepek_filename(instance, filename):
        _, ext = os.path.splitext(filename)
        ext = ext.lower()
        basename = instance.slug
        dir = 'blog_kepek'
        return os.path.join(dir, '{}{}'.format(basename, ext))


class Tartalom(models.Model):
    slug = models.SlugField(unique=True)
    text = MarkupField(blank=True)

    def __unicode__(self):
        return self.slug


class ProjectItem(models.Model):
	slug = models.SlugField(unique=True)
	pic = models.FileField(upload_to=gallery_kepek_filename, blank=True)
	description = MarkupField(blank=True)
	order = models.IntegerField(null=True, blank=True)

	def __unicode__(self):
		return self.slug

	class Meta:
		ordering = ['order']


class WorkItem(models.Model):
    slug = models.SlugField(unique=True)
    pic = models.FileField(upload_to=gallery_kepek_filename, blank=True)
    description = MarkupField(blank=True)
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.slug

    class Meta:
        ordering = ['order']


class Kepek(models.Model):
    slug = models.SlugField(unique=True)
    kep_file = models.FileField(upload_to=gallery_kepek_filename, blank=True)

    def __unicode__(self):
        return self.slug
        

class NagyKepek(models.Model):
    slug = models.SlugField(unique=True)
    kep_file = models.FileField(upload_to=blog_kepek_filename, blank=True)

    def __unicode__(self):
        return self.slug


