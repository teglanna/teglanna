from django.contrib import admin
from .models import Tartalom, Kepek, ProjectItem, WorkItem, NagyKepek

admin.site.register(Tartalom)
admin.site.register(Kepek)
admin.site.register(ProjectItem)
admin.site.register(WorkItem)
admin.site.register(NagyKepek)



