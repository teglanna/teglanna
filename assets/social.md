- **DJANGO GIRLS BUDAPEST**</br>
I'm a co-organizer of a free programming workshops for women.</br>
[djangogirls.org/budapest](http://djangogirls.org/budapest "djangogirls.org/budapest")</br>
</br>
- **PYTHON MEETUP, PYLVAX GROUP**</br>
I help to organize events for Python community as meetups and learning groups: </br>[meetup.com/budapest-py](http://meetup.com/budapest-py "meetup.com/budapest-py")</br>
[pylvax.com](http://pylvax.com "pylvax.com")			