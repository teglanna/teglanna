PROJECTS
--------

### WEBSITE / BLOG EDITING

Django website for Pylvax workshops: [pylvax.com](http://pylvax.com "pylvax.com")</br>
artwork site: [teglanna.github.io](http://teglanna.github.io "teglanna.github.io")</br>
coding site: [teglanna.pythonanywhere.com](http://teglanna.pythonanywhere.com "teglanna.pythonanywhere.com")</br>
Wordpress site for my architect mother: [teglassy.hu](http://teglassy.hu "teglassy.hu")</br>
editing and creating illustrations to a tale blog: [tundemamamesei.blogspot.hu](http://tundemamamesei.blogspot.hu "tundemamamesei.blogspot.hu")</br>
my first website: [megoldasproblemakra.hu](http://megoldasproblemakra.hu "megoldasproblemakra.hu")


### Social Events
**DJANGO GIRLS BUDAPEST | 2015**</br>
co-organizer of a free programming workshops for women</br>
[djangogirls.org/budapest](http://djangogirls.org/budapest/ "djangogirls.org/budapest/")

**PYTHON MEETUP, BUDAPEST | 2015** </br>
co-organizer</br>
[meetup.com/budapest-py](http://meetup.com/budapest-py "meetup.com/budapest-py")

**PYLVAX GROUP, BUDAPEST | 2015** </br>
co-organizer of a Python learning group</br>
[pylvax.com](http://pylvax.com "pylvax.com")a