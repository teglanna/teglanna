#### SOFTWARE SUMMER INTERN AT PULILAB, BUDAPEST, AUG - SEPT 2015
</br>
I was developing a hybrid mobile android app with JavaScript, JQuery, Cordova, Onsen UI + Angular.js and Django REST</br>
_[medium.com/the-pulilab-team
](http://medium.com/the-pulilab-team "medium.com/the-pulilab-team")_



#### EVS IN ISTANBUL, ART TEACHER</br> 2014
</br>
I participated in the program of European Voluntary Service, held arts and crafts, theater workshops and operated the foundation's website.</br>
_[basaksanatvakfi.org.tr](http://www.basaksanatvakfi.org.tr "basaksanatvakfi.org.tr")_


#### ART WORKSHOPS AND CAMPS CO-ORGANIZER, VARGA IMRE GALLERY, BUDAPEST | 2012 - 2015</br></br>
I held arts and crafts sessions and summer camps for youth.</br>
_[facebook.com/Alkotonapok](http://facebook.com/Alkotonapok "facebook.com/Alkotonapok")_


#### DRAWING TEACHER AT VERES PETER SECONDARY SCHOOL, BUDAPEST  2010-2012</br></br>
I was teaching drawing lesson and organizing exhibitons and workshops.</br>
_[verespg.hu](http://verespg.hu "verespg.hu")_