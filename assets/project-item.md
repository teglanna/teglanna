Django website for a Python learning group</br>
[pylvax.com](http://pylvax.com "pylvax.com")

Django based site for code snippets and learning stuffs</br>
[teglanna.pythonanywhere.com](http://teglanna.pythonanywhere.com "teglanna.pythonanywhere.com")

Static website for my artworks</br>
[teglanna.github.io](http://teglanna.github.io "teglanna.github.io")

Wordpress site for my architect mother</br>
[teglassy.hu](http://teglassy.hu "teglassy.hu")

Editing a tale blog and creating illustrations to it</br>
[tundemamamesei.blogspot.hu](http://tundemamamesei.blogspot.hu "tundemamamesei.blogspot.hu")

My first static website</br>
[megoldasproblemakra.hu](http://megoldasproblemakra.hu "megoldasproblemakra.hu")proje