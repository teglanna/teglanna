# Resume site written in Django #

It was running on a Vultr server, I was very happy to manage an own server and put my site there, however it wasn't contain as much data that it was worth for running it.

Therefore I have created a simple static version of this site.

You can find it here:
[annateglassy.com](http://annateglassy.com)

and here:
[github.com/teglanna/teglanna.github.io](https://github.com/teglanna/teglanna.github.io)