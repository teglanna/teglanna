--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--



SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


--
-- Name: pylvax_app_blog; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_blog (
    id integer NOT NULL,
    datum timestamp with time zone NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    kep character varying(100) NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_blog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_blog_id_seq OWNED BY pylvax_app_blog.id;


--
-- Name: pylvax_app_feladat; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_feladat (
    id integer NOT NULL,
    datum date NOT NULL,
    slug character varying(50) NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    megoldas text NOT NULL,
    _megoldas_rendered text NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_feladat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_feladat_id_seq OWNED BY pylvax_app_feladat.id;


--
-- Name: pylvax_app_homeitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_homeitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    szoveg text NOT NULL,
    "order" integer,
    _szoveg_rendered text NOT NULL
);


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_homeitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_homeitem_id_seq OWNED BY pylvax_app_homeitem.id;


--
-- Name: pylvax_app_kepek; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_kepek (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    kep_file character varying(100) NOT NULL
);


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_kepek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_kepek_id_seq OWNED BY pylvax_app_kepek.id;


--
-- Name: pylvax_app_menuitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_menuitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    text character varying(100) NOT NULL,
    "order" integer
);


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_menuitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_menuitem_id_seq OWNED BY pylvax_app_menuitem.id;


--
-- Name: pylvax_app_tartalom; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_tartalom (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    szoveg text NOT NULL,
    _szoveg_rendered text NOT NULL
);


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_tartalom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_tartalom_id_seq OWNED BY pylvax_app_tartalom.id;


--
-- Name: pylvax_app_tematika; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pylvax_app_tematika (
    id integer NOT NULL,
    datum date NOT NULL,
    slug character varying(50) NOT NULL,
    cim character varying(100) NOT NULL,
    leiras text NOT NULL,
    _leiras_rendered text NOT NULL
);


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pylvax_app_tematika_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pylvax_app_tematika_id_seq OWNED BY pylvax_app_tematika.id;


--
-- Name: teglanna_app_kepek; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teglanna_app_kepek (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    kep_file character varying(100) NOT NULL
);


--
-- Name: teglanna_app_kepek_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teglanna_app_kepek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teglanna_app_kepek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teglanna_app_kepek_id_seq OWNED BY teglanna_app_kepek.id;


--
-- Name: teglanna_app_nagykepek; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teglanna_app_nagykepek (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    kep_file character varying(100) NOT NULL
);


--
-- Name: teglanna_app_nagykepek_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teglanna_app_nagykepek_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teglanna_app_nagykepek_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teglanna_app_nagykepek_id_seq OWNED BY teglanna_app_nagykepek.id;


--
-- Name: teglanna_app_projectitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teglanna_app_projectitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    pic character varying(100) NOT NULL,
    description text NOT NULL,
    "order" integer,
    _description_rendered text NOT NULL
);


--
-- Name: teglanna_app_project_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teglanna_app_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teglanna_app_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teglanna_app_project_id_seq OWNED BY teglanna_app_projectitem.id;


--
-- Name: teglanna_app_tartalom; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teglanna_app_tartalom (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    text text NOT NULL,
    _text_rendered text NOT NULL
);


--
-- Name: teglanna_app_tartalom_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teglanna_app_tartalom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teglanna_app_tartalom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teglanna_app_tartalom_id_seq OWNED BY teglanna_app_tartalom.id;


--
-- Name: teglanna_app_workitem; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE teglanna_app_workitem (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    pic character varying(100) NOT NULL,
    description text NOT NULL,
    "order" integer,
    _description_rendered text NOT NULL
);


--
-- Name: teglanna_app_workitem_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE teglanna_app_workitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: teglanna_app_workitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE teglanna_app_workitem_id_seq OWNED BY teglanna_app_workitem.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_blog ALTER COLUMN id SET DEFAULT nextval('pylvax_app_blog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_feladat ALTER COLUMN id SET DEFAULT nextval('pylvax_app_feladat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_homeitem ALTER COLUMN id SET DEFAULT nextval('pylvax_app_homeitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_kepek ALTER COLUMN id SET DEFAULT nextval('pylvax_app_kepek_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_menuitem ALTER COLUMN id SET DEFAULT nextval('pylvax_app_menuitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_tartalom ALTER COLUMN id SET DEFAULT nextval('pylvax_app_tartalom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pylvax_app_tematika ALTER COLUMN id SET DEFAULT nextval('pylvax_app_tematika_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teglanna_app_kepek ALTER COLUMN id SET DEFAULT nextval('teglanna_app_kepek_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teglanna_app_nagykepek ALTER COLUMN id SET DEFAULT nextval('teglanna_app_nagykepek_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teglanna_app_projectitem ALTER COLUMN id SET DEFAULT nextval('teglanna_app_project_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teglanna_app_tartalom ALTER COLUMN id SET DEFAULT nextval('teglanna_app_tartalom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY teglanna_app_workitem ALTER COLUMN id SET DEFAULT nextval('teglanna_app_workitem_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add menu item	7	add_menuitem
20	Can change menu item	7	change_menuitem
21	Can delete menu item	7	delete_menuitem
22	Can add home item	8	add_homeitem
23	Can change home item	8	change_homeitem
24	Can delete home item	8	delete_homeitem
25	Can add tartalom	9	add_tartalom
26	Can change tartalom	9	change_tartalom
27	Can delete tartalom	9	delete_tartalom
28	Can add feladat	10	add_feladat
29	Can change feladat	10	change_feladat
30	Can delete feladat	10	delete_feladat
31	Can add tematika	11	add_tematika
32	Can change tematika	11	change_tematika
33	Can delete tematika	11	delete_tematika
34	Can add kepek	12	add_kepek
35	Can change kepek	12	change_kepek
36	Can delete kepek	12	delete_kepek
37	Can add blog	13	add_blog
38	Can change blog	13	change_blog
39	Can delete blog	13	delete_blog
46	Can add tartalom	16	add_tartalom
47	Can change tartalom	16	change_tartalom
48	Can delete tartalom	16	delete_tartalom
55	Can add kepek	19	add_kepek
56	Can change kepek	19	change_kepek
57	Can delete kepek	19	delete_kepek
70	Can add project item	24	add_projectitem
71	Can change project item	24	change_projectitem
72	Can delete project item	24	delete_projectitem
73	Can add work item	25	add_workitem
74	Can change work item	25	change_workitem
75	Can delete work item	25	delete_workitem
76	Can add nagy kepek	26	add_nagykepek
77	Can change nagy kepek	26	change_nagykepek
78	Can delete nagy kepek	26	delete_nagykepek
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_permission_id_seq', 78, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$20000$mPYgao2YtGL7$796icTAWGfTcQZoAiIJ7EDG+qamytMGdbSj1Qo/c3nU=	2015-09-22 16:25:22.795438+02	t	pylvax				t	t	2015-09-21 14:07:17.311139+02
2	pbkdf2_sha256$20000$F3pe1sKur9Cy$AUmG96rP8Md8V66E9VOr25k+pwu1bDMaHKYbyn+fgg4=	2015-09-24 19:07:50.378465+02	t	teglanna			teglanna@gmail.com	t	t	2015-09-24 18:55:52.57709+02
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2015-09-21 14:11:29.246772+02	1	Főoldal	1		7	1
2	2015-09-21 14:12:14.651502+02	2	Új vagy?	1		7	1
3	2015-09-21 14:12:38.5768+02	3	Tematika	1		7	1
4	2015-09-21 14:12:53.733595+02	4	Feladatok	1		7	1
5	2015-09-21 14:13:34.861952+02	5	Hasznos infó	1		7	1
6	2015-09-21 14:13:52.119568+02	6	Képek	1		7	1
7	2015-09-21 14:14:46.464413+02	7	Kapcsolat	1		7	1
8	2015-09-21 14:17:19.070584+02	1	Főoldal	2	slug módosítva.	7	1
9	2015-09-21 14:17:25.225666+02	2	Új vagy?	2	Egy mező sem változott.	7	1
10	2015-09-21 14:17:29.115715+02	3	Tematika	2	Egy mező sem változott.	7	1
11	2015-09-21 14:17:33.059089+02	4	Feladatok	2	Egy mező sem változott.	7	1
12	2015-09-21 14:17:37.236904+02	5	Hasznos infó	2	Egy mező sem változott.	7	1
13	2015-09-21 14:17:42.849552+02	7	Kapcsolat	2	Egy mező sem változott.	7	1
14	2015-09-21 14:20:51.760898+02	1	hello	1		8	1
15	2015-09-21 14:23:14.720312+02	2	uj_vagy	1		8	1
16	2015-09-21 14:24:43.411974+02	3	segits	1		8	1
17	2015-09-21 14:25:26.935083+02	4	tematika	1		8	1
18	2015-09-21 14:26:02.089987+02	5	feladatok	1		8	1
19	2015-09-21 14:28:24.383231+02	6	hasznos	1		8	1
20	2015-09-21 14:28:49.581876+02	5	feladatok	2	szoveg módosítva.	8	1
21	2015-09-21 14:29:11.187876+02	1	hello	2	szoveg és order módosítva.	8	1
22	2015-09-21 14:29:47.958336+02	2	uj_vagy	2	szoveg és order módosítva.	8	1
23	2015-09-21 14:30:13.466966+02	3	segits	2	szoveg és order módosítva.	8	1
24	2015-09-21 14:30:25.500018+02	4	tematika	2	szoveg és order módosítva.	8	1
25	2015-09-21 14:30:34.388762+02	5	feladatok	2	szoveg és order módosítva.	8	1
26	2015-09-21 14:30:42.712339+02	6	hasznos	2	szoveg és order módosítva.	8	1
27	2015-09-21 14:43:53.709786+02	1	hasznos	1		9	1
28	2015-09-21 14:52:19.848179+02	1	Home	2	text módosítva.	7	1
29	2015-09-21 14:52:56.187465+02	2	Newbies	2	slug és text módosítva.	7	1
30	2015-09-21 14:53:52.468948+02	3	Topics	2	slug és text módosítva.	7	1
31	2015-09-21 14:54:01.419998+02	4	Tasks	2	slug és text módosítva.	7	1
32	2015-09-21 14:55:04.968058+02	5	Resources	2	slug és text módosítva.	7	1
33	2015-09-21 14:55:32.146199+02	5	Info	2	slug és text módosítva.	7	1
34	2015-09-21 14:55:52.050316+02	6	Pics	2	slug és text módosítva.	7	1
35	2015-09-21 14:56:02.388295+02	7	Contact	2	slug és text módosítva.	7	1
36	2015-09-21 15:08:35.921226+02	1	hasznos	2	szoveg módosítva.	9	1
37	2015-09-21 15:10:31.230983+02	2	Newbies	2	slug módosítva.	7	1
38	2015-09-21 15:12:50.728044+02	1	hasznos	2	szoveg módosítva.	9	1
39	2015-09-21 15:13:00.535158+02	1	info	2	slug és szoveg módosítva.	9	1
40	2015-09-21 15:13:00.714883+02	1	hello	2	szoveg módosítva.	8	1
41	2015-09-21 15:16:59.187288+02	6	info	2	slug és szoveg módosítva.	8	1
42	2015-09-21 15:19:01.146013+02	2	newbie	2	slug és szoveg módosítva.	8	1
43	2015-09-21 15:19:35.725926+02	5	feladatok	2	szoveg módosítva.	8	1
44	2015-09-21 15:19:47.236435+02	5	tasks	2	slug és szoveg módosítva.	8	1
45	2015-09-21 15:23:34.613184+02	3	help	2	slug és szoveg módosítva.	8	1
46	2015-09-21 15:24:32.452234+02	4	topics	2	slug és szoveg módosítva.	8	1
47	2015-09-21 15:52:56.103674+02	1	home	2	slug és szoveg módosítva.	8	1
48	2015-09-21 16:16:50.090209+02	1	monty	1		12	1
49	2015-09-21 17:01:32.546531+02	2	newbie	1		9	1
50	2015-09-21 17:03:54.094207+02	2	newbie	2	szoveg módosítva.	9	1
51	2015-09-21 21:06:18.549653+02	2	newbie	2	szoveg módosítva.	8	1
52	2015-09-21 21:08:46.026317+02	2	newbie	2	szoveg módosítva.	9	1
53	2015-09-21 21:10:04.816959+02	2	newbie	2	szoveg módosítva.	9	1
54	2015-09-21 22:58:43.368689+02	1	09/16	1		10	1
55	2015-09-21 23:00:25.161865+02	2	09/02	1		10	1
56	2015-09-21 23:00:36.915383+02	1	09/16	2	slug, leiras és megoldas módosítva.	10	1
57	2015-09-21 23:23:42.388818+02	1	Beautiful Soup	1		11	1
58	2015-09-21 23:25:01.374409+02	2	09/16	1		11	1
59	2015-09-21 23:25:12.604465+02	1	09/02	2	cim és leiras módosítva.	11	1
60	2015-09-21 23:33:37.285356+02	2	JSON	2	cim és leiras módosítva.	11	1
61	2015-09-21 23:33:47.760249+02	1	BeautifulSoup	2	cim és leiras módosítva.	11	1
62	2015-09-21 23:34:07.750325+02	2	BeautifulSoup	2	cim, leiras és megoldas módosítva.	10	1
63	2015-09-21 23:34:18.623087+02	1	JSON	2	cim, leiras és megoldas módosítva.	10	1
64	2015-09-22 13:02:29.311496+02	1	info	2	szoveg módosítva.	9	1
65	2015-09-22 13:06:53.722296+02	1	info	2	szoveg módosítva.	9	1
66	2015-09-22 13:10:13.68375+02	1	info	2	szoveg módosítva.	9	1
67	2015-09-22 13:11:30.350884+02	1	info	2	szoveg módosítva.	9	1
68	2015-09-22 13:18:32.648315+02	1	info	2	szoveg módosítva.	9	1
69	2015-09-22 13:20:14.496114+02	1	info	2	szoveg módosítva.	9	1
70	2015-09-22 13:21:43.594834+02	1	info	2	szoveg módosítva.	9	1
71	2015-09-22 13:22:57.35134+02	1	info	2	szoveg módosítva.	9	1
72	2015-09-22 13:27:21.584404+02	1	info	2	szoveg módosítva.	9	1
73	2015-09-22 13:28:33.250237+02	1	info	2	szoveg módosítva.	9	1
74	2015-09-22 13:31:11.816671+02	1	info	2	szoveg módosítva.	9	1
75	2015-09-22 13:37:02.639647+02	1	info	2	szoveg módosítva.	9	1
76	2015-09-22 13:38:25.824149+02	1	info	2	szoveg módosítva.	9	1
77	2015-09-22 13:40:01.416851+02	1	info	2	szoveg módosítva.	9	1
78	2015-09-22 13:40:44.629573+02	1	info	2	szoveg módosítva.	9	1
79	2015-09-22 13:41:18.878506+02	1	info	2	szoveg módosítva.	9	1
80	2015-09-22 13:42:03.499585+02	1	info	2	szoveg módosítva.	9	1
81	2015-09-22 13:42:54.646754+02	1	info	2	szoveg módosítva.	9	1
82	2015-09-22 13:45:17.897007+02	1	info	2	szoveg módosítva.	9	1
83	2015-09-22 13:45:38.559932+02	1	info	2	szoveg módosítva.	9	1
84	2015-09-22 13:46:07.95898+02	1	info	2	szoveg módosítva.	9	1
85	2015-09-22 13:55:28.621967+02	1	info	2	szoveg módosítva.	9	1
86	2015-09-22 13:58:25.939384+02	1	info	2	szoveg módosítva.	9	1
87	2015-09-22 13:59:11.398254+02	1	info	2	szoveg módosítva.	9	1
88	2015-09-22 14:01:49.260434+02	1	info	2	szoveg módosítva.	9	1
89	2015-09-22 14:26:03.458715+02	1	monty	3		12	1
90	2015-09-22 14:27:08.746742+02	2	pylvax1	1		12	1
91	2015-09-22 14:27:20.970393+02	3	pylvax2	1		12	1
92	2015-09-22 14:27:31.025024+02	4	pylvax3	1		12	1
93	2015-09-22 14:27:39.083112+02	5	pylvax4	1		12	1
94	2015-09-22 14:27:49.158118+02	6	pylvax5	1		12	1
104	2015-09-25 05:01:10.507208+02	1	home	1		16	2
106	2015-09-25 05:05:25.878357+02	1	home	2	Changed szoveg.	16	2
135	2015-09-25 19:26:48.245251+02	1	home	3		16	2
136	2015-09-25 19:27:04.730994+02	2	whoami	1		16	2
147	2015-09-25 19:44:10.193741+02	1	some_pic	1		19	2
148	2015-09-25 19:45:53.192211+02	2	kutyak	1		19	2
149	2015-09-25 19:46:14.048203+02	2	kutyak	2	Changed kep_file.	19	2
150	2015-09-25 19:46:41.031715+02	3	medve	1		19	2
151	2015-09-25 19:47:10.764931+02	4	bundi	1		19	2
156	2015-09-26 13:48:35.372923+02	2	whoami	2	Changed text.	16	2
157	2015-09-26 14:58:05.693301+02	2	whoami	2	Changed text.	16	2
158	2015-09-26 15:05:20.573415+02	2	whoami	2	Changed text.	16	2
159	2015-09-26 15:07:45.157327+02	2	whoami	2	Changed text.	16	2
160	2015-09-26 15:08:03.519573+02	2	whoami	2	Changed text.	16	2
166	2015-09-26 16:01:06.221374+02	3	before	1		16	2
167	2015-09-26 16:10:11.66683+02	4	after	1		16	2
168	2015-09-26 16:11:16.998378+02	4	after	2	Changed text.	16	2
175	2015-09-28 08:19:39.793838+02	1	pylvax	2	Changed description.	24	2
176	2015-09-28 08:25:42.50545+02	1	pylvax	2	Changed description.	24	2
177	2015-09-28 08:38:14.001833+02	1	pylvax	2	Changed description.	24	2
178	2015-09-28 08:44:47.188567+02	2	tundemama	1		24	2
179	2015-09-28 08:50:29.690613+02	1	pylvax	3		24	2
180	2015-09-28 08:50:34.798607+02	2	tundemama	3		24	2
181	2015-09-28 08:50:56.360992+02	3	pylvax	1		24	2
182	2015-09-28 08:53:20.353634+02	3	pylvax	3		24	2
183	2015-09-28 08:54:15.24946+02	4	pylvax	1		24	2
184	2015-09-28 09:40:12.354666+02	5	tundemama	1		24	2
185	2015-09-28 09:41:32.24283+02	4	pylvax	2	Changed description and order.	24	2
186	2015-09-28 09:44:23.591542+02	6	paint	1		24	2
187	2015-09-28 09:45:34.387003+02	7	megoldas	1		24	2
188	2015-09-28 10:54:52.865579+02	7	megoldas	2	Changed pic and description.	24	2
189	2015-09-28 10:55:05.14199+02	7	megoldas	2	Changed pic and description.	24	2
190	2015-09-28 10:55:43.394083+02	4	pylvax	2	Changed pic and description.	24	2
191	2015-09-28 10:55:54.6571+02	4	pylvax	2	Changed pic and description.	24	2
192	2015-09-28 11:00:21.322812+02	6	paint	2	Changed pic and description.	24	2
193	2015-09-28 11:00:31.26446+02	6	paint	2	Changed pic and description.	24	2
194	2015-09-28 11:02:20.182437+02	6	paint	2	Changed pic and description.	24	2
195	2015-09-28 11:02:29.178206+02	6	paint	2	Changed pic and description.	24	2
196	2015-09-28 11:04:04.007134+02	5	tundemama	2	Changed pic and description.	24	2
197	2015-09-28 11:04:14.497483+02	5	tundemama	2	Changed pic and description.	24	2
198	2015-09-28 11:08:17.356409+02	8	teglassy	1		24	2
199	2015-09-28 11:10:50.370096+02	9	code	1		24	2
200	2015-09-28 18:22:50.873292+02	4	pylvax	2	Changed description.	24	2
201	2015-09-28 18:23:54.92903+02	6	paint	2	Changed description.	24	2
202	2015-09-28 18:25:12.407678+02	5	tundemama	2	Changed description.	24	2
203	2015-09-28 18:25:58.15281+02	7	megoldas	2	Changed description.	24	2
204	2015-09-28 18:26:39.893569+02	9	code	2	Changed description.	24	2
205	2015-09-28 18:26:53.195032+02	9	code	2	Changed description and order.	24	2
206	2015-09-28 18:27:48.536415+02	8	teglassy	2	Changed description.	24	2
207	2015-09-28 18:28:02.599469+02	7	megoldas	2	Changed description and order.	24	2
208	2015-09-28 18:28:17.97137+02	8	teglassy	2	Changed description and order.	24	2
209	2015-09-28 19:36:13.132776+02	3	before	2	Changed text.	16	2
210	2015-09-28 19:40:31.435514+02	3	before	2	Changed text.	16	2
211	2015-09-28 19:43:05.216584+02	3	before	2	Changed text.	16	2
212	2015-09-28 19:43:47.057394+02	3	before	2	Changed text.	16	2
213	2015-09-28 19:47:11.764124+02	2	whoami	2	Changed text.	16	2
214	2015-09-28 19:47:51.913928+02	2	whoami	2	Changed text.	16	2
215	2015-09-28 20:12:24.21425+02	1	pulilab	1		25	2
216	2015-09-28 20:12:44.018251+02	2	evs	1		25	2
217	2015-09-28 20:13:05.361634+02	3	varga	1		25	2
218	2015-09-28 20:13:20.350228+02	4	veres	1		25	2
219	2015-09-28 20:30:20.152088+02	5	social	1		16	2
220	2015-09-28 21:06:20.73957+02	2	whoami	2	Changed text.	16	2
221	2015-09-28 21:07:02.451192+02	2	whoami	2	Changed text.	16	2
222	2015-09-28 21:09:52.942791+02	5	social	2	Changed text.	16	2
223	2015-09-28 21:10:05.574765+02	5	social	2	Changed text.	16	2
224	2015-10-01 17:45:59.461612+02	4	bundi	2	Changed kep_file.	19	2
225	2015-10-01 17:46:42.582802+02	4	bundi	2	Changed kep_file.	19	2
226	2015-10-01 17:46:53.632905+02	4	bundi	2	No fields changed.	19	2
227	2015-10-01 17:47:19.180467+02	3	medve	2	Changed kep_file.	19	2
228	2015-10-01 17:47:38.83188+02	3	medve	2	Changed kep_file.	19	2
229	2015-10-01 17:47:55.147675+02	2	kutyak	2	Changed kep_file.	19	2
230	2015-10-01 17:48:11.555157+02	2	kutyak	2	Changed kep_file.	19	2
231	2015-10-01 17:48:24.919776+02	1	some_pic	3		19	2
232	2015-10-01 17:48:51.154664+02	5	egykutya	1		19	2
233	2015-10-01 17:52:12.661585+02	5	egykutya	2	Changed kep_file.	19	2
234	2015-10-01 17:52:44.69962+02	5	egykutya	2	Changed kep_file.	19	2
235	2015-10-01 18:08:18.13518+02	6	zao	1		19	2
236	2015-10-01 18:08:34.08219+02	7	ezso	1		19	2
237	2015-10-01 18:08:50.235926+02	8	cao	1		19	2
238	2015-10-01 18:09:14.585635+02	9	susnyas	1		19	2
239	2015-10-02 11:01:36.192502+02	1	bundi	1		26	2
240	2015-10-02 16:51:25.539095+02	3	before	2	Changed text.	16	2
241	2015-10-02 16:52:58.115967+02	3	before	2	Changed text.	16	2
242	2015-10-02 16:53:30.583342+02	3	before	2	Changed text.	16	2
243	2015-10-02 16:54:56.220298+02	1	pulilab	2	Changed description.	25	2
244	2015-10-02 16:55:43.329451+02	2	evs	2	Changed description.	25	2
245	2015-10-02 16:56:36.60798+02	3	varga	2	Changed description.	25	2
246	2015-10-02 16:57:07.805678+02	3	varga	2	Changed description.	25	2
247	2015-10-02 16:57:41.920082+02	4	veres	2	Changed description.	25	2
248	2015-10-02 16:58:45.5276+02	3	varga	2	Changed description.	25	2
249	2015-10-02 16:59:31.294203+02	1	pulilab	2	Changed description.	25	2
250	2015-10-02 17:00:05.833692+02	2	evs	2	Changed description.	25	2
251	2015-10-02 17:00:53.037367+02	3	varga	2	Changed description.	25	2
252	2015-10-02 17:01:14.268173+02	3	varga	2	Changed description.	25	2
253	2015-10-02 17:01:55.523292+02	3	varga	2	Changed description.	25	2
254	2015-10-02 17:02:14.561155+02	4	veres	2	Changed description.	25	2
255	2015-10-02 17:02:28.048634+02	4	veres	2	Changed description.	25	2
256	2015-10-02 17:04:19.804969+02	4	veres	2	Changed description.	25	2
257	2015-10-02 17:05:00.452757+02	2	evs	2	Changed description.	25	2
258	2015-10-02 17:05:40.149431+02	2	evs	2	Changed description.	25	2
259	2015-10-02 17:06:19.624046+02	2	evs	2	Changed description.	25	2
260	2015-10-02 17:06:39.059765+02	2	evs	2	Changed description.	25	2
261	2015-10-02 17:06:58.323553+02	2	evs	2	Changed description.	25	2
262	2015-10-04 20:30:36.691707+02	2	whoami	2	Changed text.	16	2
263	2015-10-04 20:31:06.902785+02	2	whoami	2	Changed text.	16	2
264	2015-10-04 20:31:29.313926+02	2	whoami	2	Changed text.	16	2
265	2015-10-04 20:57:08.322348+02	2	whoami	2	Changed text.	16	2
266	2015-10-04 20:58:04.405184+02	2	whoami	2	Changed text.	16	2
267	2015-10-04 20:59:06.911282+02	2	whoami	2	Changed text.	16	2
268	2015-10-04 21:00:22.669877+02	2	whoami	2	Changed text.	16	2
269	2015-10-04 21:27:36.152846+02	2	whoami	2	Changed text.	16	2
270	2015-10-04 21:57:05.581281+02	5	social	2	Changed text.	16	2
271	2015-10-04 22:00:18.090317+02	5	social	2	Changed text.	16	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 271, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	pylvax_app	menuitem
8	pylvax_app	homeitem
9	pylvax_app	tartalom
10	pylvax_app	feladat
11	pylvax_app	tematika
12	pylvax_app	kepek
13	pylvax_app	blog
16	teglanna_app	tartalom
19	teglanna_app	kepek
24	teglanna_app	projectitem
25	teglanna_app	workitem
26	teglanna_app	nagykepek
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_content_type_id_seq', 26, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2015-09-21 14:04:36.110162+02
2	auth	0001_initial	2015-09-21 14:04:36.191335+02
3	admin	0001_initial	2015-09-21 14:04:36.222689+02
4	contenttypes	0002_remove_content_type_name	2015-09-21 14:04:36.296052+02
5	auth	0002_alter_permission_name_max_length	2015-09-21 14:04:36.319499+02
6	auth	0003_alter_user_email_max_length	2015-09-21 14:04:36.353407+02
7	auth	0004_alter_user_username_opts	2015-09-21 14:04:36.373949+02
8	auth	0005_alter_user_last_login_null	2015-09-21 14:04:36.399247+02
9	auth	0006_require_contenttypes_0002	2015-09-21 14:04:36.401436+02
10	pylvax_app	0001_initial	2015-09-21 14:04:36.532562+02
11	sessions	0001_initial	2015-09-21 14:04:36.547281+02
12	teglanna_app	0001_initial	2015-09-24 19:01:50.918951+02
13	teglanna_app	0002_auto_20150925_0248	2015-09-25 04:48:31.65981+02
14	teglanna_app	0003_auto_20150925_0311	2015-09-25 05:11:44.991203+02
15	teglanna_app	0004_learningitem	2015-09-25 05:29:24.327415+02
16	teglanna_app	0005_auto_20150925_0334	2015-09-25 05:34:29.894747+02
17	teglanna_app	0006_auto_20150925_0355	2015-09-25 05:55:53.0385+02
18	teglanna_app	0007_project	2015-09-28 08:07:07.85356+02
19	teglanna_app	0008_auto_20150928_0619	2015-09-28 08:19:10.084547+02
20	teglanna_app	0009_auto_20150928_0650	2015-09-28 08:50:11.649216+02
21	teglanna_app	0010_auto_20150928_0652	2015-09-28 08:52:55.342011+02
22	teglanna_app	0011_workitem	2015-09-28 20:11:05.63237+02
23	teglanna_app	0012_nagykepek	2015-10-02 10:59:30.806205+02
24	teglanna_app	0013_auto_20151002_1652	2015-10-02 18:53:03.00767+02
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_migrations_id_seq', 24, true);


--
-- Data for Name: pylvax_app_blog; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_blog (id, datum, cim, leiras, kep, _leiras_rendered) FROM stdin;
\.


--
-- Name: pylvax_app_blog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_blog_id_seq', 1, false);


--
-- Data for Name: pylvax_app_feladat; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_feladat (id, datum, slug, cim, leiras, megoldas, _megoldas_rendered, _leiras_rendered) FROM stdin;
2	2015-09-02	task0902	BeautifulSoup	Beautiful Soup	```\r\nfrom bs4 import BeautifulSoup\r\nimport codecs\r\n\r\n\r\ndef read_file_contents(path):\r\n    with codecs.open(path, encoding='utf-8') as infile:\r\n        return infile.read().strip()\r\n\r\nkiralyok_str = read_file_contents('kiralyok.html')\r\n\r\n\r\nsoup = BeautifulSoup(kiralyok_str, 'html.parser')\r\n\r\nh3_list = soup.find_all('h3')\r\nh3 = h3_list[0]\r\nkiralyhaz = h3.find('span').text\r\n\r\nkorszakok = list()\r\n\r\nfor h3 in h3_list:\r\n    span = h3.find('span', {'class': 'mw-headline'})\r\n    if span:\r\n        korszak = span.text\r\n        table = h3.next_sibling.next_sibling\r\n\r\n        korszakok.append((korszak, table))\r\n\r\n```\r\n	<pre><code>from bs4 import BeautifulSoup\nimport codecs\n\n\ndef read_file_contents(path):\n    with codecs.open(path, encoding='utf-8') as infile:\n        return infile.read().strip()\n\nkiralyok_str = read_file_contents('kiralyok.html')\n\n\nsoup = BeautifulSoup(kiralyok_str, 'html.parser')\n\nh3_list = soup.find_all('h3')\nh3 = h3_list[0]\nkiralyhaz = h3.find('span').text\n\nkorszakok = list()\n\nfor h3 in h3_list:\n    span = h3.find('span', {'class': 'mw-headline'})\n    if span:\n        korszak = span.text\n        table = h3.next_sibling.next_sibling\n\n        korszakok.append((korszak, table))\n\n</code></pre>\n	<p>Beautiful Soup</p>\n
1	2015-09-16	task0916	JSON	JSON forecast	```\r\nimport requests\r\n\r\nlongitude = 19.05\r\nlatitude = 47.53\r\napikey = '2e4f0d2961984011b1eaf8ad0ca8489d'\r\n\r\nurl_template = 'https://api.forecast.io/forecast/{api}/{lat},{lon}'\r\nurl = url_template.format(api=apikey, lat=latitude, lon=longitude)\r\n\r\nprint url\r\n\r\nr = requests.get(url)\r\nunits = 'si'\r\n\r\nif r.ok:\r\n    data = r.json()\r\n\r\n\r\nr2 = requests.get(url + '?units=si')\r\nif r2.ok:\r\n    data_si = r.json()\r\n\r\nparams_dict = {\r\n    'units': 'si',\r\n    'lang': 'fr'\r\n}\r\n\r\nr3 = requests.get(url, params=params_dict)\r\nif r3.ok:\r\n    data_si2 = r.json()\r\n```\r\n	<pre><code>import requests\n\nlongitude = 19.05\nlatitude = 47.53\napikey = '2e4f0d2961984011b1eaf8ad0ca8489d'\n\nurl_template = 'https://api.forecast.io/forecast/{api}/{lat},{lon}'\nurl = url_template.format(api=apikey, lat=latitude, lon=longitude)\n\nprint url\n\nr = requests.get(url)\nunits = 'si'\n\nif r.ok:\n    data = r.json()\n\n\nr2 = requests.get(url + '?units=si')\nif r2.ok:\n    data_si = r.json()\n\nparams_dict = {\n    'units': 'si',\n    'lang': 'fr'\n}\n\nr3 = requests.get(url, params=params_dict)\nif r3.ok:\n    data_si2 = r.json()\n</code></pre>\n	<p>JSON forecast</p>\n
\.


--
-- Name: pylvax_app_feladat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_feladat_id_seq', 2, true);


--
-- Data for Name: pylvax_app_homeitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_homeitem (id, slug, szoveg, "order", _szoveg_rendered) FROM stdin;
6	info	What else?\r\n--------------\r\nYou can continue learning at home, we collected useful stuffs, great tutorials and books.\r\n	6	<h2>What else?</h2>\n<p>You can continue learning at home, we collected useful stuffs, great tutorials and books.</p>\n
5	tasks	What we did so far\r\n---------------------\r\nYou can find here the materials of past lessons and homeworks. \r\n	5	<h2>What we did so far</h2>\n<p>You can find here the materials of past lessons and homeworks.</p>\n
3	help	You can also help!\r\n-------------\r\nIf you are a coding ninja, or a developer with some experiences, you are also welcome. <br>\r\nGive lessons, help the beginners, and perhaps you will also learn something new!\r\n	3	<h2>You can also help!</h2>\n<p>If you are a coding ninja, or a developer with some experiences, you are also welcome. <br>\nGive lessons, help the beginners, and perhaps you will also learn something new!</p>\n
4	topics	What we learn\r\n-------------\r\nWe are learning in two separated groups, Dániel Ábel helps to the newcomes to get into the flow.</br>\r\nZsolti Erő shows more and more magic to the old hands.</br>\r\nThe details you can find here.\r\n	4	<h2>What we learn</h2>\n<p>We are learning in two separated groups, Dániel Ábel helps to the newcomes to get into the flow.</br>\nZsolti Erő shows more and more magic to the old hands.</br>\nThe details you can find here.</p>\n
1	home	Hello!\r\n------\r\nWelcome! Pylvax is a workshop, where you can learn coding even if you are totally beginner and haven't seen any lines of code before. </br>\r\nWe meet every wednesday, we code together and share our experiences with each other.\r\n	1	<h2>Hello!</h2>\n<p>Welcome! Pylvax is a workshop, where you can learn coding even if you are totally beginner and haven't seen any lines of code before. </br>\nWe meet every wednesday, we code together and share our experiences with each other.</p>\n
2	newbie	Don't know how to code? No problem!\r\n-----------------------------\r\nWe don't expect you to have any previous experience in coding or in the IT field.<br>\r\nYou can take the first steps even at home!\r\n	2	<h2>Don't know how to code? No problem!</h2>\n<p>We don't expect you to have any previous experience in coding or in the IT field.<br>\nYou can take the first steps even at home!</p>\n
\.


--
-- Name: pylvax_app_homeitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_homeitem_id_seq', 6, true);


--
-- Data for Name: pylvax_app_kepek; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_kepek (id, slug, kep_file) FROM stdin;
2	pylvax1	gallery_kepek/pylvax1.jpg
3	pylvax2	gallery_kepek/pylvax2.jpg
4	pylvax3	gallery_kepek/pylvax3.jpg
5	pylvax4	gallery_kepek/pylvax4.jpg
6	pylvax5	gallery_kepek/pylvax5.jpg
\.


--
-- Name: pylvax_app_kepek_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_kepek_id_seq', 6, true);


--
-- Data for Name: pylvax_app_menuitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_menuitem (id, slug, text, "order") FROM stdin;
1	home	Home	1
3	topics	Topics	3
4	tasks	Tasks	4
5	info	Info	5
6	pics	Pics	6
7	contact	Contact	7
2	newbie	Newbies	2
\.


--
-- Name: pylvax_app_menuitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_menuitem_id_seq', 7, true);


--
-- Data for Name: pylvax_app_tartalom; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_tartalom (id, slug, szoveg, _szoveg_rendered) FROM stdin;
2	newbie	First steps\r\n==================\r\nPay attention to your assignments, practice at home, talk with developers and you'll see that your skills will improve sooner than you would think!\r\n\r\nYou are gonna need to install the following stuffs for getting started:\r\n\r\n**Windows**\r\n\r\n**Python** (use 32-bit)</br>\r\n[https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi](http://)\r\n\r\n**Sublime Text 2** (use 32-bit)</br>\r\n[http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20Setup.exe](http://)</br></br>\r\nTotal Commander</br>\r\n[http://totalcmd2.s3.amazonaws.com/tcmd851ax32_64.exe](http://)</br></br>\r\n\r\n**Cmder**</br>\r\n[https://github.com/bliker/cmder/releases/download/v1.1.4.1/cmder.zip](http://)</br></br>\r\n\r\n**Path Editor**</br>\r\n[http://rix0r.nl/downloads/windowspatheditor/windowspatheditor-1.3.zip](http://)\r\n</br></br>\r\n\r\n**Ipython**</br>\r\nipython‑3.0.0‑py27‑none‑any.whl</br>\r\n[http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython](http://)\r\n</br></br>\r\n	<h1>First steps</h1>\n<p>Pay attention to your assignments, practice at home, talk with developers and you'll see that your skills will improve sooner than you would think!</p>\n<p>You are gonna need to install the following stuffs for getting started:</p>\n<p><strong>Windows</strong></p>\n<p><strong>Python</strong> (use 32-bit)</br>\n<a href="http://">https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi</a></p>\n<p><strong>Sublime Text 2</strong> (use 32-bit)</br>\n<a href="http://">http://c758482.r82.cf2.rackcdn.com/Sublime%20Text%202.0.2%20Setup.exe</a></br></br>\nTotal Commander</br>\n<a href="http://">http://totalcmd2.s3.amazonaws.com/tcmd851ax32_64.exe</a></br></br></p>\n<p><strong>Cmder</strong></br>\n<a href="http://">https://github.com/bliker/cmder/releases/download/v1.1.4.1/cmder.zip</a></br></br></p>\n<p><strong>Path Editor</strong></br>\n<a href="http://">http://rix0r.nl/downloads/windowspatheditor/windowspatheditor-1.3.zip</a>\n</br></br></p>\n<p><strong>Ipython</strong></br>\nipython‑3.0.0‑py27‑none‑any.whl</br>\n<a href="http://">http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython</a>\n</br></br></p>\n
1	info	<p class="info_title">Here we collected many information for you to help learning.</p>\r\n</br>\r\n<p class="y_line">\r\nIntro to Computer Science\r\n</p>\r\nFirst an amazing course, which explains the very basics of programming and Python language:</br>\r\n\r\n[www.udacity.com/course/cs101](https://www.udacity.com/course/cs101)\r\n\r\n<p class="b_line">\r\nCodecademy / Python</p>\r\n\r\nCodecademy is useful to practice the newly learned stuffs.</br>\r\n[www.codecademy.com/tracks/python](https://www.codecademy.com/tracks/python)\r\n\r\n<p class="y_line">\r\nLearn Python the hard way</p>\r\n\r\nA great online book teaches you from fundamentals, shows how to use command line, and helps to take the first steps.</br>\r\n The language it is written, awesome! :)</br>\r\n[learnpythonthehardway.org/book/](http://learnpythonthehardway.org/book/)\r\n\r\n<p class="b_line">Programming Foundations with Python</p>\r\n\r\nIf you want to know more about object oriented programming, you can start with simple but great exercises.</br>\r\n[www.udacity.com/course/ud036](http://www.udacity.com/course/ud036)\r\n\r\n<p class="y_line">Exercism</p>\r\n\r\nThe idea behind the site is pretty simple: write code for all kinds of problems, submit it, get feedback for it from other users, and give feedback to them. You'll get to see how others solve problems.</br>\r\n[exercism.io](http://exercism.io/)\r\n\r\n<p class="b_line">Intro to Git and Github</p>\r\nThis course can teach you to understand version control and shows how to collaborate with other developers.</br>\r\n\r\n[www.udacity.com/course/how-to-use-git-and-github](http://www.udacity.com/course/how-to-use-git-and-github--ud775)\r\n\r\n<p class="y_line">\r\nAn Introduction to Interactive Programming in Python</p>\r\nIt is  partially enabled course, you have to check, when it is opened, but also good, and from basics.</br>\r\n\r\n[www.coursera.org/course/interactivepython1\r\n](http://www.coursera.org/course/interactivepython1)\r\n\r\nIt has a second part as well:</br>\r\n[www.coursera.org/course/interactivepython2](http://www.coursera.org/course/interactivepython2)\r\n\r\n\r\n	<p class="info_title">Here we collected many information for you to help learning.</p>\n</br>\n<p class="y_line">\nIntro to Computer Science\n</p>\nFirst an amazing course, which explains the very basics of programming and Python language:</br>\n<p><a href="https://www.udacity.com/course/cs101">www.udacity.com/course/cs101</a></p>\n<p class="b_line">\nCodecademy / Python</p>\n<p>Codecademy is useful to practice the newly learned stuffs.</br>\n<a href="https://www.codecademy.com/tracks/python">www.codecademy.com/tracks/python</a></p>\n<p class="y_line">\nLearn Python the hard way</p>\n<p>A great online book teaches you from fundamentals, shows how to use command line, and helps to take the first steps.</br>\nThe language it is written, awesome! :)</br>\n<a href="http://learnpythonthehardway.org/book/">learnpythonthehardway.org/book/</a></p>\n<p class="b_line">Programming Foundations with Python</p>\n<p>If you want to know more about object oriented programming, you can start with simple but great exercises.</br>\n<a href="http://www.udacity.com/course/ud036">www.udacity.com/course/ud036</a></p>\n<p class="y_line">Exercism</p>\n<p>The idea behind the site is pretty simple: write code for all kinds of problems, submit it, get feedback for it from other users, and give feedback to them. You'll get to see how others solve problems.</br>\n<a href="http://exercism.io/">exercism.io</a></p>\n<p class="b_line">Intro to Git and Github</p>\nThis course can teach you to understand version control and shows how to collaborate with other developers.</br>\n<p><a href="http://www.udacity.com/course/how-to-use-git-and-github--ud775">www.udacity.com/course/how-to-use-git-and-github</a></p>\n<p class="y_line">\nAn Introduction to Interactive Programming in Python</p>\nIt is  partially enabled course, you have to check, when it is opened, but also good, and from basics.</br>\n<p><a href="http://www.coursera.org/course/interactivepython1">www.coursera.org/course/interactivepython1\n</a></p>\n<p>It has a second part as well:</br>\n<a href="http://www.coursera.org/course/interactivepython2">www.coursera.org/course/interactivepython2</a></p>\n
\.


--
-- Name: pylvax_app_tartalom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_tartalom_id_seq', 2, true);


--
-- Data for Name: pylvax_app_tematika; Type: TABLE DATA; Schema: public; Owner: -
--

COPY pylvax_app_tematika (id, datum, slug, cim, leiras, _leiras_rendered) FROM stdin;
2	2015-09-16	topic0916	JSON	With JSON you can structure your data.	<p>With JSON you can structure your data.</p>\n
1	2015-09-02	topic0902	BeautifulSoup	With BeautifulSoup you can scrape the html and get huge amount of data.	<p>With BeautifulSoup you can scrape the html and get huge amount of data.</p>\n
\.


--
-- Name: pylvax_app_tematika_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('pylvax_app_tematika_id_seq', 2, true);


--
-- Data for Name: teglanna_app_kepek; Type: TABLE DATA; Schema: public; Owner: -
--

COPY teglanna_app_kepek (id, slug, kep_file) FROM stdin;
4	bundi	gallery_kepek/bundi.jpg
3	medve	gallery_kepek/medve.png
2	kutyak	gallery_kepek/kutyak.jpg
5	egykutya	gallery_kepek/egykutya.jpg
6	zao	gallery_kepek/zao.jpg
7	ezso	gallery_kepek/ezso.jpg
8	cao	gallery_kepek/cao.jpg
9	susnyas	gallery_kepek/susnyas.jpg
\.


--
-- Name: teglanna_app_kepek_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('teglanna_app_kepek_id_seq', 9, true);


--
-- Data for Name: teglanna_app_nagykepek; Type: TABLE DATA; Schema: public; Owner: -
--

COPY teglanna_app_nagykepek (id, slug, kep_file) FROM stdin;
1	bundi	blog_kepek/bundi.jpg
\.


--
-- Name: teglanna_app_nagykepek_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('teglanna_app_nagykepek_id_seq', 1, true);


--
-- Name: teglanna_app_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('teglanna_app_project_id_seq', 9, true);


--
-- Data for Name: teglanna_app_projectitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY teglanna_app_projectitem (id, slug, pic, description, "order", _description_rendered) FROM stdin;
7	megoldas	gallery_kepek/megoldas.png	My first static website</br>\r\n[megoldasproblemakra.hu](http://megoldasproblemakra.hu "megoldasproblemakra.hu")	6	<p>My first static website</br>\n<a href="http://megoldasproblemakra.hu" title="megoldasproblemakra.hu">megoldasproblemakra.hu</a></p>\n
8	teglassy	gallery_kepek/teglassy.png	Wordpress site for my architect mother</br>\r\n[teglassy.hu](http://teglassy.hu "teglassy.hu")	4	<p>Wordpress site for my architect mother</br>\n<a href="http://teglassy.hu" title="teglassy.hu">teglassy.hu</a></p>\n
4	pylvax	gallery_kepek/pylvax.png	Django website for a Python learning group</br>\r\n[pylvax.com](http://pylvax.com "pylvax.com")\r\n	1	<p>Django website for a Python learning group</br>\n<a href="http://pylvax.com" title="pylvax.com">pylvax.com</a></p>\n
6	paint	gallery_kepek/paint.png	Static website for my artworks</br>\r\n[teglanna.github.io](http://teglanna.github.io "teglanna.github.io")	3	<p>Static website for my artworks</br>\n<a href="http://teglanna.github.io" title="teglanna.github.io">teglanna.github.io</a></p>\n
5	tundemama	gallery_kepek/tundemama.png	Editing a tale blog and creating illustrations to it</br>\r\n[tundemamamesei.blogspot.hu](http://tundemamamesei.blogspot.hu "tundemamamesei.blogspot.hu")	4	<p>Editing a tale blog and creating illustrations to it</br>\n<a href="http://tundemamamesei.blogspot.hu" title="tundemamamesei.blogspot.hu">tundemamamesei.blogspot.hu</a></p>\n
9	code	gallery_kepek/code.png	Django based site for code snippets and learning stuffs</br>\r\n[teglanna.pythonanywhere.com](http://teglanna.pythonanywhere.com "teglanna.pythonanywhere.com")	3	<p>Django based site for code snippets and learning stuffs</br>\n<a href="http://teglanna.pythonanywhere.com" title="teglanna.pythonanywhere.com">teglanna.pythonanywhere.com</a></p>\n
\.


--
-- Data for Name: teglanna_app_tartalom; Type: TABLE DATA; Schema: public; Owner: -
--

COPY teglanna_app_tartalom (id, slug, text, _text_rendered) FROM stdin;
4	after	PROJECTS\r\n--------\r\n\r\n### WEBSITE / BLOG EDITING\r\n\r\nDjango website for Pylvax workshops: [pylvax.com](http://pylvax.com "pylvax.com")</br>\r\nartwork site: [teglanna.github.io](http://teglanna.github.io "teglanna.github.io")</br>\r\ncoding site: [teglanna.pythonanywhere.com](http://teglanna.pythonanywhere.com "teglanna.pythonanywhere.com")</br>\r\nWordpress site for my architect mother: [teglassy.hu](http://teglassy.hu "teglassy.hu")</br>\r\nediting and creating illustrations to a tale blog: [tundemamamesei.blogspot.hu](http://tundemamamesei.blogspot.hu "tundemamamesei.blogspot.hu")</br>\r\nmy first website: [megoldasproblemakra.hu](http://megoldasproblemakra.hu "megoldasproblemakra.hu")\r\n\r\n\r\n### Social Events\r\n**DJANGO GIRLS BUDAPEST | 2015**</br>\r\nco-organizer of a free programming workshops for women</br>\r\n[djangogirls.org/budapest](http://djangogirls.org/budapest/ "djangogirls.org/budapest/")\r\n\r\n**PYTHON MEETUP, BUDAPEST | 2015** </br>\r\nco-organizer</br>\r\n[meetup.com/budapest-py](http://meetup.com/budapest-py "meetup.com/budapest-py")\r\n\r\n**PYLVAX GROUP, BUDAPEST | 2015** </br>\r\nco-organizer of a Python learning group</br>\r\n[pylvax.com](http://pylvax.com "pylvax.com")\r\n	<h2>PROJECTS</h2>\n<h3>WEBSITE / BLOG EDITING</h3>\n<p>Django website for Pylvax workshops: <a href="http://pylvax.com" title="pylvax.com">pylvax.com</a></br>\nartwork site: <a href="http://teglanna.github.io" title="teglanna.github.io">teglanna.github.io</a></br>\ncoding site: <a href="http://teglanna.pythonanywhere.com" title="teglanna.pythonanywhere.com">teglanna.pythonanywhere.com</a></br>\nWordpress site for my architect mother: <a href="http://teglassy.hu" title="teglassy.hu">teglassy.hu</a></br>\nediting and creating illustrations to a tale blog: <a href="http://tundemamamesei.blogspot.hu" title="tundemamamesei.blogspot.hu">tundemamamesei.blogspot.hu</a></br>\nmy first website: <a href="http://megoldasproblemakra.hu" title="megoldasproblemakra.hu">megoldasproblemakra.hu</a></p>\n<h3>Social Events</h3>\n<p><strong>DJANGO GIRLS BUDAPEST | 2015</strong></br>\nco-organizer of a free programming workshops for women</br>\n<a href="http://djangogirls.org/budapest/" title="djangogirls.org/budapest/">djangogirls.org/budapest</a></p>\n<p><strong>PYTHON MEETUP, BUDAPEST | 2015</strong> </br>\nco-organizer</br>\n<a href="http://meetup.com/budapest-py" title="meetup.com/budapest-py">meetup.com/budapest-py</a></p>\n<p><strong>PYLVAX GROUP, BUDAPEST | 2015</strong> </br>\nco-organizer of a Python learning group</br>\n<a href="http://pylvax.com" title="pylvax.com">pylvax.com</a></p>\n
2	whoami	Hi,\r\n---\r\n\r\nI'm Anna, junior web developer with intern-level experience in software-dev as well. </br>\r\nOfficially my background is fine art that is why I am super excited about UX and training daily basis myself to bond my IT and artistic interest.</br>\r\n<p>\r\n</br>\r\n\r\n### About this site\r\n\r\nIn the :before menu you can find my jobs and my intern, and the ::after contains what I did after I realized I have something to do with html,ccs and coding. </br>\r\n</br>\r\n	<h2>Hi,</h2>\n<p>I'm Anna, junior web developer with intern-level experience in software-dev as well. </br>\nOfficially my background is fine art that is why I am super excited about UX and training daily basis myself to bond my IT and artistic interest.</br></p>\n<p>\n</br>\n<h3>About this site</h3>\n<p>In the :before menu you can find my jobs and my intern, and the ::after contains what I did after I realized I have something to do with html,ccs and coding. </br>\n</br></p>\n
5	social	- **DJANGO GIRLS BUDAPEST**</br>\r\nI'm a co-organizer of a free programming workshops for women.</br>\r\n[djangogirls.org/budapest](http://djangogirls.org/budapest "djangogirls.org/budapest")</br>\r\n</br>\r\n- **PYTHON MEETUP, PYLVAX GROUP**</br>\r\nI help to organize events for Python community as meetups and learning groups: </br>[meetup.com/budapest-py](http://meetup.com/budapest-py "meetup.com/budapest-py")</br>\r\n[pylvax.com](http://pylvax.com "pylvax.com")\t\t\t	<ul>\n<li><strong>DJANGO GIRLS BUDAPEST</strong></br>\nI'm a co-organizer of a free programming workshops for women.</br>\n<a href="http://djangogirls.org/budapest" title="djangogirls.org/budapest">djangogirls.org/budapest</a></br>\n</br></li>\n<li><strong>PYTHON MEETUP, PYLVAX GROUP</strong></br>\nI help to organize events for Python community as meetups and learning groups: </br><a href="http://meetup.com/budapest-py" title="meetup.com/budapest-py">meetup.com/budapest-py</a></br>\n<a href="http://pylvax.com" title="pylvax.com">pylvax.com</a></li>\n</ul>\n
3	before	WORK EXPERIENCES\r\n----------------\r\n\r\n#### SOFTWARE SUMMER INTERN AT PULILAB BUDAPEST, AUG - SEPT 2015\r\n\r\nI was developing a hybrid mobile android app with JavaScript, JQuery, Cordova, Onsen UI + Angular.js and Django REST</br>\r\n_[medium.com/the-pulilab-team\r\n](http://medium.com/the-pulilab-team "medium.com/the-pulilab-team")_</br></br>\r\n**EVS IN ISTANBUL | 2014**\r\n</br>\r\nI participated in the program of European Voluntary Service, held arts and crafts, theater workshops and operated the foundation's website.</br>\r\n_[basaksanatvakfi.org.tr](http://www.basaksanatvakfi.org.tr "basaksanatvakfi.org.tr")_</br></br>\r\n\r\n**ART WORKSHOPS AND CAMPS CO-ORGANIZER | 2012 - 2015**</br>\r\n**VARGA IMRE GALLERY, VASARELY MUSEUM, BUDAPEST**</br>\r\nI held arts and crafts sessions and summer camps for youth.</br>\r\n_[facebook.com/Alkotonapok](http://facebook.com/Alkotonapok "facebook.com/Alkotonapok")_</br></br>\r\n\r\n**DRAWING TEACHER AT VERES PETER SECONDARY SCHOOL, BUDAPEST | 2010-2012**</br>\r\nI was teaching drawing lesson and organizing exhibitons and workshops.</br>\r\n_[verespg.hu](http://verespg.hu "verespg.hu")_	<h2>WORK EXPERIENCES</h2>\n<h4>SOFTWARE SUMMER INTERN AT PULILAB BUDAPEST, AUG - SEPT 2015</h4>\n<p>I was developing a hybrid mobile android app with JavaScript, JQuery, Cordova, Onsen UI + Angular.js and Django REST</br>\n<em><a href="http://medium.com/the-pulilab-team" title="medium.com/the-pulilab-team">medium.com/the-pulilab-team\n</a></em></br></br>\n<strong>EVS IN ISTANBUL | 2014</strong>\n</br>\nI participated in the program of European Voluntary Service, held arts and crafts, theater workshops and operated the foundation's website.</br>\n<em><a href="http://www.basaksanatvakfi.org.tr" title="basaksanatvakfi.org.tr">basaksanatvakfi.org.tr</a></em></br></br></p>\n<p><strong>ART WORKSHOPS AND CAMPS CO-ORGANIZER | 2012 - 2015</strong></br>\n<strong>VARGA IMRE GALLERY, VASARELY MUSEUM, BUDAPEST</strong></br>\nI held arts and crafts sessions and summer camps for youth.</br>\n<em><a href="http://facebook.com/Alkotonapok" title="facebook.com/Alkotonapok">facebook.com/Alkotonapok</a></em></br></br></p>\n<p><strong>DRAWING TEACHER AT VERES PETER SECONDARY SCHOOL, BUDAPEST | 2010-2012</strong></br>\nI was teaching drawing lesson and organizing exhibitons and workshops.</br>\n<em><a href="http://verespg.hu" title="verespg.hu">verespg.hu</a></em></p>\n
\.


--
-- Name: teglanna_app_tartalom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('teglanna_app_tartalom_id_seq', 5, true);


--
-- Data for Name: teglanna_app_workitem; Type: TABLE DATA; Schema: public; Owner: -
--

COPY teglanna_app_workitem (id, slug, pic, description, "order", _description_rendered) FROM stdin;
2	evs		#### EVS IN ISTANBUL, ART TEACHER</br> 2014\r\n</br>\r\nI participated in the program of European Voluntary Service, held arts and crafts, theater workshops and operated the foundation's website.</br>\r\n_[basaksanatvakfi.org.tr](http://www.basaksanatvakfi.org.tr "basaksanatvakfi.org.tr")_	2	<h4>EVS IN ISTANBUL, ART TEACHER</br> 2014</h4>\n<p></br>\nI participated in the program of European Voluntary Service, held arts and crafts, theater workshops and operated the foundation's website.</br>\n<em><a href="http://www.basaksanatvakfi.org.tr" title="basaksanatvakfi.org.tr">basaksanatvakfi.org.tr</a></em></p>\n
3	varga		#### ART WORKSHOPS AND CAMPS CO-ORGANIZER, VARGA IMRE GALLERY, BUDAPEST | 2012 - 2015</br></br>\r\nI held arts and crafts sessions and summer camps for youth.</br>\r\n_[facebook.com/Alkotonapok](http://facebook.com/Alkotonapok "facebook.com/Alkotonapok")_	3	<h4>ART WORKSHOPS AND CAMPS CO-ORGANIZER, VARGA IMRE GALLERY, BUDAPEST | 2012 - 2015</br></br></h4>\n<p>I held arts and crafts sessions and summer camps for youth.</br>\n<em><a href="http://facebook.com/Alkotonapok" title="facebook.com/Alkotonapok">facebook.com/Alkotonapok</a></em></p>\n
4	veres		#### DRAWING TEACHER AT VERES PETER SECONDARY SCHOOL, BUDAPEST  2010-2012</br></br>\r\nI was teaching drawing lesson and organizing exhibitons and workshops.</br>\r\n_[verespg.hu](http://verespg.hu "verespg.hu")_	4	<h4>DRAWING TEACHER AT VERES PETER SECONDARY SCHOOL, BUDAPEST  2010-2012</br></br></h4>\n<p>I was teaching drawing lesson and organizing exhibitons and workshops.</br>\n<em><a href="http://verespg.hu" title="verespg.hu">verespg.hu</a></em></p>\n
1	pulilab		#### SOFTWARE SUMMER INTERN AT PULILAB, BUDAPEST, AUG - SEPT 2015\r\n</br>\r\nI was developing a hybrid mobile android app with JavaScript, JQuery, Cordova, Onsen UI + Angular.js and Django REST</br>\r\n_[medium.com/the-pulilab-team\r\n](http://medium.com/the-pulilab-team "medium.com/the-pulilab-team")_	1	<h4>SOFTWARE SUMMER INTERN AT PULILAB, BUDAPEST, AUG - SEPT 2015</h4>\n<p></br>\nI was developing a hybrid mobile android app with JavaScript, JQuery, Cordova, Onsen UI + Angular.js and Django REST</br>\n<em><a href="http://medium.com/the-pulilab-team" title="medium.com/the-pulilab-team">medium.com/the-pulilab-team\n</a></em></p>\n
\.


--
-- Name: teglanna_app_workitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('teglanna_app_workitem_id_seq', 4, true);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_45f3b1d93ec8c61c_uniq; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_45f3b1d93ec8c61c_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: pylvax_app_blog_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_blog
    ADD CONSTRAINT pylvax_app_blog_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_feladat_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_feladat
    ADD CONSTRAINT pylvax_app_feladat_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_feladat_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_feladat
    ADD CONSTRAINT pylvax_app_feladat_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_homeitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_homeitem
    ADD CONSTRAINT pylvax_app_homeitem_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_homeitem_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_homeitem
    ADD CONSTRAINT pylvax_app_homeitem_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_kepek_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_kepek
    ADD CONSTRAINT pylvax_app_kepek_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_kepek_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_kepek
    ADD CONSTRAINT pylvax_app_kepek_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_menuitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_menuitem
    ADD CONSTRAINT pylvax_app_menuitem_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_menuitem_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_menuitem
    ADD CONSTRAINT pylvax_app_menuitem_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_tartalom_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tartalom
    ADD CONSTRAINT pylvax_app_tartalom_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_tartalom_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tartalom
    ADD CONSTRAINT pylvax_app_tartalom_slug_key UNIQUE (slug);


--
-- Name: pylvax_app_tematika_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tematika
    ADD CONSTRAINT pylvax_app_tematika_pkey PRIMARY KEY (id);


--
-- Name: pylvax_app_tematika_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pylvax_app_tematika
    ADD CONSTRAINT pylvax_app_tematika_slug_key UNIQUE (slug);


--
-- Name: teglanna_app_kepek_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_kepek
    ADD CONSTRAINT teglanna_app_kepek_pkey PRIMARY KEY (id);


--
-- Name: teglanna_app_kepek_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_kepek
    ADD CONSTRAINT teglanna_app_kepek_slug_key UNIQUE (slug);


--
-- Name: teglanna_app_nagykepek_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_nagykepek
    ADD CONSTRAINT teglanna_app_nagykepek_pkey PRIMARY KEY (id);


--
-- Name: teglanna_app_nagykepek_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_nagykepek
    ADD CONSTRAINT teglanna_app_nagykepek_slug_key UNIQUE (slug);


--
-- Name: teglanna_app_project_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_projectitem
    ADD CONSTRAINT teglanna_app_project_pkey PRIMARY KEY (id);


--
-- Name: teglanna_app_project_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_projectitem
    ADD CONSTRAINT teglanna_app_project_slug_key UNIQUE (slug);


--
-- Name: teglanna_app_tartalom_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_tartalom
    ADD CONSTRAINT teglanna_app_tartalom_pkey PRIMARY KEY (id);


--
-- Name: teglanna_app_tartalom_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_tartalom
    ADD CONSTRAINT teglanna_app_tartalom_slug_key UNIQUE (slug);


--
-- Name: teglanna_app_workitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_workitem
    ADD CONSTRAINT teglanna_app_workitem_pkey PRIMARY KEY (id);


--
-- Name: teglanna_app_workitem_slug_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY teglanna_app_workitem
    ADD CONSTRAINT teglanna_app_workitem_slug_key UNIQUE (slug);


--
-- Name: auth_group_name_253ae2a6331666e8_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_name_253ae2a6331666e8_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_0e939a4f ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_group_permissions_8373b171 ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_permission_417f1b1c ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_0e939a4f; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_0e939a4f ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_groups_e8701ad4 ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_8373b171; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_8373b171 ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_e8701ad4 ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_51b3b110094b8aae_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX auth_user_username_51b3b110094b8aae_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_417f1b1c; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_417f1b1c ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_e8701ad4; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_admin_log_e8701ad4 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_de54fa62; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_de54fa62 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_461cfeaa630ca218_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX django_session_session_key_461cfeaa630ca218_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: pylvax_app_feladat_slug_432dc727f62aa986_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_feladat_slug_432dc727f62aa986_like ON pylvax_app_feladat USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_homeitem_slug_10ea7e995e676944_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_homeitem_slug_10ea7e995e676944_like ON pylvax_app_homeitem USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_kepek_slug_56b5e88aefd10d9f_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_kepek_slug_56b5e88aefd10d9f_like ON pylvax_app_kepek USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_menuitem_slug_523500fc103527f0_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_menuitem_slug_523500fc103527f0_like ON pylvax_app_menuitem USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_tartalom_slug_48e09f2003a9ff58_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_tartalom_slug_48e09f2003a9ff58_like ON pylvax_app_tartalom USING btree (slug varchar_pattern_ops);


--
-- Name: pylvax_app_tematika_slug_12a667d48da646_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX pylvax_app_tematika_slug_12a667d48da646_like ON pylvax_app_tematika USING btree (slug varchar_pattern_ops);


--
-- Name: teglanna_app_kepek_slug_e74631f_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX teglanna_app_kepek_slug_e74631f_like ON teglanna_app_kepek USING btree (slug varchar_pattern_ops);


--
-- Name: teglanna_app_nagykepek_slug_445c1fa8_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX teglanna_app_nagykepek_slug_445c1fa8_like ON teglanna_app_nagykepek USING btree (slug varchar_pattern_ops);


--
-- Name: teglanna_app_project_slug_6575deb0_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX teglanna_app_project_slug_6575deb0_like ON teglanna_app_projectitem USING btree (slug varchar_pattern_ops);


--
-- Name: teglanna_app_tartalom_slug_938bcf4_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX teglanna_app_tartalom_slug_938bcf4_like ON teglanna_app_tartalom USING btree (slug varchar_pattern_ops);


--
-- Name: teglanna_app_workitem_slug_70a0875a_like; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX teglanna_app_workitem_slug_70a0875a_like ON teglanna_app_workitem USING btree (slug varchar_pattern_ops);


--
-- Name: auth_content_type_id_508cf46651277a81_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_content_type_id_508cf46651277a81_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_group_id_689710a9a73b7457_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user__permission_id_384b62483d7071f0_fk_auth_permission_id FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permiss_user_id_7f0938558328534a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djan_content_type_id_697914295151027a_fk_django_content_type_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT djan_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

