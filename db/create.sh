#!/usr/bin/env sh

DB_NAME=tegla

dropdb $DB_NAME --if-exists
dropuser $DB_NAME
createuser $DB_NAME --createdb
createdb -O $DB_NAME -U $DB_NAME $DB_NAME
